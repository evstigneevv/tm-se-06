package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.Collection;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String userId, final String projectId, final String taskName) throws EmptyStringException {
        if (userId == null || projectId == null || taskName == null || userId.isEmpty() || projectId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.create(userId, taskName, projectId);
    }

    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Collection<Task> findAllByUserId(final String userId) throws EmptyStringException {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findAllByUserId(userId);
    }

    public boolean remove(final String userId, final String taskId) throws EmptyStringException {
        if (userId == null || taskId == null || userId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.remove(userId, taskId);
    }

    public Collection<Task> getTaskListByProjectId(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.getTaskListByProjectId(userId, projectId);
    }

    public Task update(final String userId, final String taskId, final String taskName) throws EmptyStringException {
        if (userId == null || taskId == null || taskName == null || userId.isEmpty() || taskId.isEmpty() || taskName.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.update(userId, taskId, taskName);
    }

    public Task findOne(final String userId, final String taskId) throws EmptyStringException {
        if (userId == null || taskId == null || userId.isEmpty() || taskId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.findOne(userId, taskId);
    }

    public Task merge(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (task == null) {
            throw new RepositoryException("No project!");
        }
        return taskRepository.merge(userId, task);
    }

    public Task persist(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (task == null) {
            throw new RepositoryException("No project!");
        }
        return taskRepository.persist(userId, task);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public boolean deleteAllProjectTasks(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.deleteAllProjectTasks(userId, projectId);
    }

    public boolean removeAllByUserId(final String userId) throws EmptyStringException {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return taskRepository.removeAllByUserId(userId);
    }

}
