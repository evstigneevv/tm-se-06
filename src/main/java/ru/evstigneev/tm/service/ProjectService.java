package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;

import java.util.Collection;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Collection<Project> findAll() throws Exception {
        if (projectRepository.findAll().isEmpty()) {
            throw new RepositoryException("No projects found!");
        }
        return projectRepository.findAll();
    }

    public Project create(final String userId, final String projectName) throws EmptyStringException {
        if (userId == null || projectName == null || userId.isEmpty() || projectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.create(projectName, userId);
    }

    public boolean remove(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.deleteAllProjectTasks(userId, projectId);
        return projectRepository.remove(userId, projectId);
    }

    public Project update(final String userId, final String projectId, final String newProjectName) throws EmptyStringException {
        if (userId == null || projectId == null || newProjectName == null || userId.isEmpty() || projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.update(userId, projectId, newProjectName);
    }

    public Project findOne(final String userId, final String projectId) throws EmptyStringException {
        if (userId == null || projectId == null || userId.isEmpty() || projectId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findOne(userId, projectId);
    }

    public Project merge(final String userId, final Project project) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.merge(userId, project);
    }

    public Project persist(final String userId, final Project project) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        if (project == null) {
            throw new RepositoryException("No project!");
        }
        return projectRepository.persist(userId, project);
    }

    public Collection<Project> findAllByUserId(final String userId) throws EmptyStringException {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return projectRepository.findAllByUserId(userId);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public boolean removeAllByUserId(String userId) throws EmptyStringException {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        taskRepository.removeAllByUserId(userId);
        return projectRepository.removeAllByUserId(userId);
    }

}
