package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.RepositoryException;
import ru.evstigneev.tm.repository.UserRepository;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(String login, String password) throws EmptyStringException {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, password);
    }

    public User create(String login, String password, RoleType role) throws EmptyStringException {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.create(login, password, role);
    }

    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public boolean checkPassword(String login, String password) throws EmptyStringException {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            throw new EmptyStringException();
        }
        try {
            Collection<User> users = findAll();
            for (User u : users) {
                if (u.getLogin().equals(login)) {
                    return Arrays.toString(MessageDigest.getInstance("MD5").digest(password.getBytes())).equals(u.getPasswordHash());
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePassword(String login, String newPassword) throws EmptyStringException {
        if (login == null || newPassword == null || login.isEmpty() || newPassword.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.updatePassword(login, newPassword);
    }

    public boolean isAdmin(User user) throws RepositoryException {
        if (user == null) {
            throw new RepositoryException("there is no user!");
        }
        return user.getRole().equals(RoleType.ADMIN);
    }

    public User findByLogin(String login) throws EmptyStringException {
        if (login == null || login.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.findByLogin(login);
    }

    public void update(String userId, String userName) throws EmptyStringException {
        if (userId == null || userName == null || userId.isEmpty() || userName.isEmpty()) {
            throw new EmptyStringException();
        }
        userRepository.update(userId, userName);
    }

    public boolean remove(String userId) throws EmptyStringException {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyStringException();
        }
        return userRepository.remove(userId);
    }

}
