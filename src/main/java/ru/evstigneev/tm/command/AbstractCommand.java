package ru.evstigneev.tm.command;

import ru.evstigneev.tm.bootstrap.Bootstrap;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.NoPermissionException;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public RoleType[] getSupportedRoles() {
        return null;
    }

    public abstract boolean requiredAuth() throws Exception;

    public boolean isAdmin() throws Exception {
        if (!bootstrap.getUserService().isAdmin(bootstrap.getCurrentUser()))
            throw new NoPermissionException();
        return true;
    }

    public boolean havePermission(String first, String second) throws Exception {
        return true;
    }

}