package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class TaskDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAT";
    }

    @Override
    public String description() {
        return "Delete all tasks";
    }

    @Override
    public void execute() throws Exception {
        isAdmin();
        bootstrap.getTaskService().removeAll();
        System.out.println("All tasks deleted!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
