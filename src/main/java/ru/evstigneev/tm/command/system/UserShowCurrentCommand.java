package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserShowCurrentCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SCU";
    }

    @Override
    public String description() {
        return "Shows current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("User ID: " + bootstrap.getCurrentUser().getId() + " | User login: " +
                bootstrap.getCurrentUser().getLogin() + " | User role: " + bootstrap.getCurrentUser().getRole());
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}