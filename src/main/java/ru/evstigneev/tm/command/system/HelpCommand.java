package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public final String command() {
        return "HELP";
    }

    @Override
    public final String description() {
        return "Show available commands and their description";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.command() + " " + command.description());
        }
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return false;
    }

}
