package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.enumerated.RoleType;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UU";
    }

    @Override
    public String description() {
        return "Update current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter new user name: ");
        String input = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().update(bootstrap.getCurrentUser().getId(), input);
        System.out.println("User name has changed!");
    }

    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}

