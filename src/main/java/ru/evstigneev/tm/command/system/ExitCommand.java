package ru.evstigneev.tm.command.system;

import ru.evstigneev.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String command() {
        return "EXIT";
    }

    @Override
    public String description() {
        return "Closes application";
    }

    @Override
    public void execute() throws Exception {
        System.exit(2);
    }

    @Override
    public boolean requiredAuth() throws Exception {
        return true;
    }

}
