package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> taskMap = new LinkedHashMap<>();

    public Task create(final String userId, final String taskName, final String projectId) {
        Task task = new Task(userId, UUID.randomUUID().toString(), projectId, taskName);
        return taskMap.put(task.getId(), task);
    }

    public boolean remove(final String userId, final String taskId) {
        if (taskMap.get(taskId).getUserId().equals(userId)) {
            return taskMap.remove(taskId) != null;
        }
        return false;
    }

    public Task update(final String userId, final String taskId, final String taskName) {
        if (taskMap.get(taskId).getUserId().equals(userId)) {
            taskMap.get(taskId).setName(taskName);
        }
        return taskMap.get(taskId);
    }

    public Collection<Task> getTaskListByProjectId(final String userId, final String projectId) {
        List<Task> taskListByProjectId = new ArrayList<>();
        Collection<Task> taskList = taskMap.values();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    public Collection<Task> findAll() {
        return taskMap.values();
    }

    public Collection<Task> findAllByUserId(final String userId) {
        Map<String, Task> userTasks = new LinkedHashMap<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userTasks.put(entry.getKey(), entry.getValue());
        }
        return userTasks.values();
    }

    public boolean deleteAllProjectTasks(final String userId, final String projectId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = taskMap.values();
        for (Task t : projectTasks) {
            if (t.getProjectId().equals(projectId) && t.getUserId().equals(userId)) {
                taskMap.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    public Task findOne(final String userId, final String taskId) {
        if (taskMap.get(taskId).getUserId().equals(userId)) {
            return taskMap.get(taskId);
        }
        return null;
    }

    public void removeAll() {
        taskMap.clear();
    }

    public boolean removeAllByUserId(String userId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = taskMap.values();
        for (Task t : projectTasks) {
            if (t.getUserId().equals(userId)) {
                taskMap.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    public Task merge(final String userId, final Task task) {
        if (findOne(userId, task.getId()) != null) {
            taskMap.put(task.getId(), task);
        }
        return persist(userId, task);
    }

    public Task persist(final String userId, final Task task) {
        if (taskMap.get(task.getId()).getUserId().equals(userId)) {
            return taskMap.put(task.getId(), task);
        }
        return null;
    }

}