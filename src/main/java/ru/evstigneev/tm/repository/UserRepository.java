package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository {

    private Map<String, User> userMap = new LinkedHashMap<>();

    public User create(String login, String password) {
        User user = new User(login, password);
        userMap.put(user.getId(), user);
        return user;
    }

    public User create(String login, String password, RoleType role) {
        User user = new User(login, password, role);
        userMap.put(user.getId(), user);
        return user;
    }

    public boolean updatePassword(String login, String newPassword) {
        Collection<User> userCollection = userMap.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                userMap.get(u.getId()).setPasswordHash(newPassword);
                return true;
            }
        }
        return false;
    }

    public User findByLogin(String login) {
        Collection<User> userCollection = userMap.values();
        for (User u : userCollection) {
            if (u.getLogin().equals(login)) {
                return u;
            }
        }
        return null;
    }

    public Collection<User> findAll() {
        return userMap.values();
    }

    public void update(String userId, String userName) {
        userMap.get(userId).setLogin(userName);
    }

    public boolean remove(String userId) {
        return userMap.remove(userId) != null;
    }

}