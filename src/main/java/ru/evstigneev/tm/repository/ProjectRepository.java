package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public Project create(final String userId, final String projectName) {
        Project project = new Project(projectName, UUID.randomUUID().toString(), userId);
        projectMap.put(project.getId(), project);
        return project;
    }

    public boolean remove(final String userId, final String projectId) {
        if (projectMap.get(projectId).getUserId().equals(userId)) {
            return projectMap.remove(projectId) != null;
        } else return false;
    }

    public Project update(final String userId, final String projectId, final String projectName) {
        if (projectMap.get(projectId).getUserId().equals(userId)) {
            projectMap.get(projectId).setName(projectName);
        }
        return projectMap.get(projectId);
    }

    public Collection<Project> findAll() {
        return projectMap.values();
    }

    public Collection<Project> findAllByUserId(final String userId) {
        Map<String, Project> userProjects = new LinkedHashMap<>();
        for (Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (entry.getValue().getUserId().equals(userId))
                userProjects.put(entry.getKey(), entry.getValue());
        }
        return userProjects.values();
    }

    public Project findOne(final String userId, final String projectId) {
        if (projectMap.get(projectId).getUserId().equals(userId)) {
            return projectMap.get(projectId);
        }
        return null;
    }

    public Project merge(final String userId, final Project project) {
        if (projectMap.get(project.getId()).getUserId().equals(userId)) {
            return projectMap.put(project.getId(), project);
        }
        return null;
    }

    public Project persist(final String userId, final Project project) {
        if (projectMap.get(project.getId()).getUserId().equals(userId)) {
            return projectMap.put(project.getId(), project);
        }
        return null;
    }

    public boolean removeAllByUserId(final String userId) {
        boolean isRemoved = false;
        for (Project p : findAllByUserId(userId)) {
            if (p.getUserId().equals(userId)) {
                projectMap.remove(p.getId());
                isRemoved = true;
            }
        }
        return isRemoved;
    }

    public void removeAll() {
        projectMap.clear();
    }

}
