package ru.evstigneev.tm.enumerated;

public enum RoleType {

    ADMIN("ADMIN"),
    USER("USER");

    String role;

    RoleType(String role) {
        this.role = role;
    }

    public String displayName() {
        return role;
    }
}
