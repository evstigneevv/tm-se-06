package ru.evstigneev.tm.bootstrap;

import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.LoginException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.service.UserService;

import java.util.*;

public class Bootstrap {

    private final Scanner scanner = new Scanner(System.in);
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(new ProjectRepository(), taskRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private final UserService userService = new UserService(new UserRepository());
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private User currentUser;

    public void init(final Class... classes) throws Exception {
        if (classes != null) {
            for (Class clazz : classes) {
                registry(clazz);
            }
        }
        userService.create("admin", "admin", RoleType.ADMIN);
        start();
    }

    private void registry(final Class clazz) {
        if (clazz.getSuperclass().equals(AbstractCommand.class)) {
            try {
                final AbstractCommand command = (AbstractCommand) clazz.newInstance();
                command.setBootstrap(this);
                registry(command);
            } catch (InstantiationException | IllegalAccessException | CommandCorruptException e) {
                e.printStackTrace();
            }
        }
    }

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String commandText = command.command();
        final String description = command.description();
        if (commandText == null || description == null || commandText.isEmpty() || description.isEmpty())
            throw new CommandCorruptException();
        commands.put(commandText, command);
    }

    public void start() {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"EXIT\" to exit...");
        while (true) {
            String command = scanner.nextLine().toUpperCase();
            try {
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new EmptyStringException();
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandCorruptException();
        }
        if (abstractCommand.requiredAuth() && currentUser == null) {
            throw new LoginException();
        }
        if (abstractCommand.requiredAuth()) {
            Collection<RoleType> roleTypeCollection = Arrays.asList(abstractCommand.getSupportedRoles());
            if (roleTypeCollection.contains(currentUser.getRole())) {
                abstractCommand.execute();
            } else {
                throw new NoPermissionException();
            }
        } else {
            abstractCommand.execute();
        }
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Scanner getScanner() {
        return scanner;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

}
