package ru.evstigneev.tm.entity;

import ru.evstigneev.tm.enumerated.RoleType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

public class User {

    private String login;
    private String id;
    private String passwordHash;
    private RoleType role;

    public User(String login, String password) {
        this.login = login;
        id = UUID.randomUUID().toString();
        setPasswordHash(password);
        role = RoleType.ADMIN;
    }

    public User(String login, String password, RoleType isAdmin) {
        this.login = login;
        id = UUID.randomUUID().toString();
        setPasswordHash(password);
        this.role = isAdmin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            passwordHash = Arrays.toString(messageDigest.digest(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
